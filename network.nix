let
  pkgs = import <nixpkgs> {};
  secrets = import ./prod-secrets;
in
{
  transitionGoals =
    let
      ports = {
        matrixAppserviceIrc = 12345;
        nodeExporter = 9001;
      };
    in
      {
        imports =
          [
            ./vultr-machine.nix
            ./matrix-appservice-irc
          ];
        deployment.targetHost = "45.76.117.249";

        services.openssh.enable = true;

        services.matrixAppserviceIrc = {
          enable = true;
          registration = secrets.appserviceRegistration;
          passkey = secrets.appservicePasskey;
          config = import ./appservice-config.nix secrets.kiwiconIrcMappings;
          port = ports.matrixAppserviceIrc;
        };

        services.nginx = {
          enable = true;

          recommendedGzipSettings = true;
          recommendedOptimisation = true;
          recommendedProxySettings = true;
          recommendedTlsSettings = true;

          virtualHosts."matrix-appservice-irc.kwiius.com" = {
            enableACME = true;
            forceSSL = true;
            locations."/" = {
              proxyPass = "http://127.0.0.1:${toString ports.matrixAppserviceIrc}";
            };
          };
        };

        services.prometheus.exporters = {
          node = {
            enable = true;
            port = ports.nodeExporter;
          };
        };

        security.acme.email = "jamie@kwiius.com";
        security.acme.acceptTerms = true;

        networking.firewall.allowedTCPPorts = [ 22 80 443 ports.nodeExporter ];

        users = {
          mutableUsers = false;

          extraUsers.root = {
            hashedPassword = "*";
          };
        };

        environment.systemPackages = [];
      };
  network.enableRollback = true;
}
