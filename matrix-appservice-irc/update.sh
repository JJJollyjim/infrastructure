#! /usr/bin/env nix-shell
#! nix-shell -i bash -p bash_5 jq nodePackages.node2nix nixpkgs-fmt
set -euo pipefail

cd "$(dirname "$0")"

CURRENT_TAG="$(nix-instantiate --json --eval -E '(builtins.head (builtins.attrValues (import ./composition.nix {}))).version' | jq -r)"
echo "Current version: $CURRENT_TAG"
NEW_TAG="$(curl -sS https://api.github.com/repos/matrix-org/matrix-appservice-irc/releases/latest | jq -r '.name')"
echo "New version: $NEW_TAG"

node2nix \
  --nodejs-12 \
  -d \
  -i <(echo '[{"matrix-appservice-irc": "git://github.com/matrix-org/matrix-appservice-irc.git#'"$NEW_TAG"'"}]') \
  -c composition.nix \
  --no-copy-node-env

RECALCULATED_TAG="$(nix-instantiate --json --eval -E '(builtins.head (builtins.attrValues (import ./composition.nix {}))).version' | jq -r)"

nixpkgs-fmt composition.nix node-packages.nix

if [ "$RECALCULATED_TAG" != "$NEW_TAG" ]; then
	echo "ERROR! The update process did not produce the correct version number. This is a bug."
	echo "CURRENT_TAG: $(CURRENT_TAG)"
	echo "NEW_TAG: $(NEW_TAG)"
	echo "RECALCULATED_TAG: $(RECALCULATED_TAG)"
	exit 1
fi

