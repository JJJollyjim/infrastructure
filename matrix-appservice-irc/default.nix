{ config, pkgs, lib, ... }:
let
  cfg = config.services.matrixAppserviceIrc;
  appservicePkg = builtins.head (builtins.attrValues (import ./composition.nix { inherit pkgs; }));
in
  with lib;

  {
    options = {
      services.matrixAppserviceIrc = {
        enable = mkOption {
          default = false;
          type = with types; bool;
          description = ''
            Run a matrix-appservice-irc daemon
          '';
        };
        registration = mkOption {
          type = with types; str;
          description = ''
            The daemon's registration file
          '';
        };
        passkey = mkOption {
          type = with types; str;
          description = ''
            The PEM private key with which to encrypt passwords in the database
          '';
        };
        port = mkOption {
          type = with types; port;
          description = ''
            The port to listen on for appservice requests from the homeserver
          '';
        };
        config = mkOption {
          type = with types; attrs;
        };
      };
    };

    config = mkIf cfg.enable {
      systemd.services.matrix-appservice-irc = {
        wantedBy = [ "multi-user.target" ];
        after = [ "network.target" ];
        description = "Start the irc client of username.";
        serviceConfig = {
          Type = "simple";

          DynamicUser = true;
          User = "matrix-appservice-irc";
          Group = "matrix-appservice-irc";

          StateDirectory = "matrix-appservice-irc";

          ExecStart = ''${appservicePkg}/bin/matrix-appservice-irc -c /etc/matrix-appservice-irc/config.yaml -f /etc/matrix-appservice-irc/registration.yaml -p ${toString cfg.port}'';
        };
      };

      environment.etc."matrix-appservice-irc/config.yaml".text = builtins.toJSON
        (lib.recursiveUpdate {
          ircService.databaseUri = "nedb:///var/lib/matrix-appservice-irc/data";
          ircService.passwordEncryptionKeyPath = "/etc/matrix-appservice-irc/passkey.pem";
        } cfg.config
        );
      environment.etc."matrix-appservice-irc/registration.yaml".text = cfg.registration;
      environment.etc."matrix-appservice-irc/passkey.pem".text = cfg.passkey;

      environment.systemPackages = [ appservicePkg ];
    };
  }
