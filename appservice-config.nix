secretKiwiconIrcMappings:
{
  homeserver = {
    domain = "johnguant.com";
    enablePresence = true;
    url = "https://johnguant.com:443";
  };
  ircService = {
    debugApi = {
      enabled = false;
      port = 11100;
    };
    ident = {
      address = "0.0.0.0";
      enabled = false;
      port = 1113;
    };
    logging = {
      level = "info";
      maxFiles = 5;
      toConsole = true;
    };
    matrixHandler = {
      eventCacheSize = 4096;
    };
    metrics = {
      enabled = true;
      remoteUserAgeBuckets = [ "1h" "1d" "1w" ];
    };
    provisioning = {
      enabled = false;
      requestTimeoutSeconds = 300;
    };
    servers = {
      "ircs.kiwicon.org" = {
        additionalAddresses = [];
        allowExpiredCerts = false;
        botConfig = {
          enabled = false;
          joinChannelsIfNoUsers = true;
          nick = "MatrixBot";
        };
        dynamicChannels = {
          aliasTemplate = "#irc_$SERVER_$CHANNEL";
          createAlias = true;
          enabled = true;
          federate = true;
          groupId = "+kiwicon:johnguant.com";
          joinRule = "public";
          published = true;
        };
        ircClients = {
          allowNickChanges = true;
          concurrentReconnectLimit = 50;
          idleTimeout = 172800;
          ipv6 = {
            only = false;
          };
          lineLimit = 3;
          maxClients = 30;
          nickTemplate = "$DISPLAY";
          reconnectIntervalMs = 5000;
        };
        mappings = secretKiwiconIrcMappings;
        matrixClients = {
          displayName = "$NICK";
          joinAttempts = -1;
          userTemplate = "@irc_$NICK";
        };
        membershipLists = {
          channels = [];
          enabled = true;
          floodDelayMs = 10000;
          global = {
            ircToMatrix = {
              incremental = true;
              initial = true;
            };
            matrixToIrc = {
              incremental = true;
              initial = true;
            };
          };
          rooms = [];
        };
        modePowerMap = {
          o = 50;
        };
        name = "Kiwicon";
        networkId = "kiwicon";
        port = 6697;
        privateMessages = {
          enabled = true;
          federate = true;
        };
        quitDebounce = {
          delayMaxMs = 7200000;
          delayMinMs = 3600000;
          enabled = false;
          quitsPerSecond = 5;
        };
        sasl = false;
        sendConnectionMessages = true;
        ssl = true;
        sslselfsign = false;
      };
    };
    bridgeInfoState = {
      enabled = true;
      initial = true;
    };
  };
}
